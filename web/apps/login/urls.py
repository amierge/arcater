from django.urls import path,re_path,reverse
from . import views

urlpatterns = [
        path('',                    views.index,               name='index'),
        path('index',               views.index,               name='index'),
        path('login',               views.login,               name='login'),
        path('register',            views.register,            name='register'),
        path('check',               views.check,               name='check')
]

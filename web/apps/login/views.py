from django.shortcuts import render,HttpResponse
from django.contrib.auth import authenticate, login

# Create your views here.
from django.contrib import auth
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect

import json
import re
from urllib import parse

def index(request):
    return render(request, 'login/index.html', {'login':"login"})

def check(request):
    if request.method == "POST":
        urldata=request.body.decode('utf-8')
        urldata = parse.unquote(urldata)
        urldata = re.sub(r"\"",'',urldata)
        urldata = re.sub(r"\{",'',urldata)
        urldata = re.sub(r"\}",'',urldata)
        temp,urldata = urldata.split('=', 1)
        name,passwd = urldata.split(',', 1)
        temp,name = name.split(':', 1)
        temp,passwd = passwd.split(':', 1)
		
        user = authenticate(username=name, password=passwd)
        if user:
            auth.login(request, user)
            if request.user.is_authenticated:
                return HttpResponse(json.dumps({"ret":"login is ok!"},ensure_ascii=False))
        else:
                return HttpResponse(json.dumps({"ret":"用户名或者密码错误!"},ensure_ascii=False))
    else:
        return HttpResponse(json.dumps({"ret":"输入错误!"},ensure_ascii=False))
	
def login(request):
    return HttpResponseRedirect('/file/index')
	
def register(request):
    if request.method == 'POST':
        urldata=request.body.decode('utf-8')
        urldata = parse.unquote(urldata)
        urldata = re.sub(r"\"",'',urldata)
        urldata = re.sub(r"\{",'',urldata)
        urldata = re.sub(r"\}",'',urldata)
        temp,urldata = urldata.split('=', 1)
        name,passwd = urldata.split(',', 1)
        temp,name = name.split(':', 1)
        temp,passwd = passwd.split(':', 1)
		
        user = authenticate(username=name, password=passwd)
        if user is not None:
            if user.is_active:
                return HttpResponse(json.dumps({"erro":"用户名已经存在!"},ensure_ascii=False))
        else:
            user = User.objects.create_user(name,"micagent@163.com",passwd)
            user.save() 
            return HttpResponse(json.dumps({"ret":"register is ok!"},ensure_ascii=False))
    else:
        return render(request, 'login/index.html', {'regist':"regist"})
	
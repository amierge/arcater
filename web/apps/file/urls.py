from django.urls import path,re_path,reverse
from . import views

urlpatterns = [
        path('',                    views.index,               name='index'),
        path('index',               views.index,               name='index'),
        path('upload',              views.upload,              name='upload'),
        path('show',                views.show,                name='show'),
        path('save_file',           views.save_file,        name='save_file')
]

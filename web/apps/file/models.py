from django.db import models
import django.utils.timezone as timezone

# Create your models here.
class stockt(models.Model):
        nid        = models.AutoField(primary_key=True, max_length=20)
        type       = models.CharField(max_length=60,null=True)
        name       = models.CharField(max_length=60,null=True)
        packag     = models.TextField(null=True)
        option     = models.TextField(null=True)
        img        = models.ImageField(upload_to='img')
        price      = models.DecimalField(max_digits=15,decimal_places=2,default=0)
        quantity   = models.IntegerField(default=0)
        #amount     = models.DecimalField(max_digits=25,decimal_places=2,default=0)
        amount     = models.IntegerField(default=0)
        createTime = models.DateTimeField('SaveData',default = timezone.now)
        modifyTime = models.DateTimeField('LastSaveData',default = timezone.now)
        remark     = models.TextField(null=True)
        Reserve0   = models.IntegerField(default=0)
        Reserve1   = models.DecimalField(max_digits=25,decimal_places=2,default=0)
        Reserve2   = models.TextField(null=True)
		
#class GfzNotice(models.Model):
#        title = models.CharField(max_length=50,null=True)
 #       content = models.TextField(null=True)


#from django.shortcuts import render
from django.shortcuts import render,redirect,HttpResponse

# Create your views here
from django.shortcuts import render, HttpResponse
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.conf import settings

from openpyxl import load_workbook
from collections import OrderedDict

from django.contrib.auth.decorators import login_required

import sys
import os
import openpyxl
import re
import json
from urllib import parse

from django import forms
from django.forms import widgets

from django.db import models
from collections import Counter
from file.models import stockt

key_name = "name"

@login_required
def index(request):
#return render(request, 'comm/index.html', {'titles':titles})
    #return render(request, 'file/index.html')
    return render(request, 'file/index.html', {'index':"index"})

@login_required
def upload(request):
    #try:
        if request.method == "POST":
            uploadfile = request.FILES['upload_file']
            uploadfilename = "%s/%s" % (settings.UPLOAD_ROOT,uploadfile.name)
            with open(uploadfilename, 'wb') as f:
                for fimg in uploadfile.chunks():
                    f.write(fimg)
            #wb = load_workbook(uploadfilename)
            wb = openpyxl.load_workbook(uploadfilename)
            sheet = wb.get_sheet_by_name('BOM')
            ws=wb.active
            rows_len =  sheet.max_row + 1
            columns_len = sheet.max_column + 1
            data = []
            for i in range(1,rows_len):			
                list = {}
                for j in range(1,columns_len): 
                    ptr = '%s' % sheet.cell(row=1, column=j).value
                    list[ptr] = sheet.cell(row=i, column=j).value
                ap = []
                for k,v in list.items():
                    if isinstance(v,float): #excel中的值默认是float,需要进行判断处理，通过'"%s":%d'，'"%s":"%s"'格式化数组
                        ap.append('"%s":%d' % (k, v))
                    else:
                        ap.append('"%s":"%s"' % (k, v)) 
                str = '{%s}' % (','.join(ap))
                data.append(str)
            rdata ='[%s]' % (','.join(data))
            with open('student4.json',"w") as f:
                f.write(rdata)
            # Clear the file
            talk = "rm -rf %s" % uploadfilename
            os.system(talk)
            dict = {"number":"Tom", "age":23},{"number":"T1om", "age":33}
            json.dumps(dict)
            #return render(request, 'file/index.html', {'columns_len': columns_len, 'dict':dict})
            return HttpResponse(rdata)
            #return HttpResponse("upload is post!")
    #except:
    #   return HttpResponse("upload is erro!")	 

	
def bytes_to_int(bytes):
    result = 0
    for b in bytes:
        result = result * 256 + int(b)
    return result

@login_required	
def save_file(request):
    stockts = stockt.objects.all()
    if request.method == "POST":
        urldata=request.body.decode('utf-8')
        urldata = parse.unquote(urldata)
        urldata = re.sub(r"\"",'',urldata) 
        temp,urldata = urldata.split('=', 1)
        cnt = urldata.count(key_name)
        type = cnt
        urldata = urldata.split('},{', cnt)

        for i in range(0, cnt):
            regex = re.compile(r"name:\w+")
            tdata = regex.search(urldata[i])
            temp,name = tdata.group(0).split(':', 1)

            quantity = re.findall(r"quantity:(\w+)",urldata[i])
            quantity = bytes_to_int(quantity)
           
            regex = re.compile(r"place:\w+,\w+")
            tdata = regex.search(urldata[i])
            temp,place = tdata.group(0).split(':', 1)

            option,temp = urldata[i].split(',packag', 1)
            temp,option = option.split(',option:', 1)

            regex = re.compile(r"packag:\w+")
            tdata = regex.search(urldata[i])
            temp,packag = tdata.group(0).split(':', 1)

            regex = re.compile(r"type:\w+")
            tdata = regex.search(urldata[i])
            temp,type = tdata.group(0).split(':', 1)

            amount = re.findall(r"amount:(\w+)",urldata[i])
            amount = bytes_to_int(amount)

            info = stockt.objects.filter(option=option, packag=packag).first()
            if info:
                amount = info.amount + amount
                info.amount = amount
                info.save()
            else: 
                file_ops = stockt.objects.create(name=name,quantity=quantity,amount=amount,option=option,packag=packag,type=type)
                file_ops.save()
			
        return HttpResponse(json.dumps({"ret":"save file is ok!"},ensure_ascii=False))
    else:
        return HttpResponse(json.dumps({"count": "fail!"}))

@login_required	
def show(request):
    file_list = stockt.objects.all()
    #return HttpResponse(json.dumps({"ret":"保存成功!"},ensure_ascii=False))
    return render(request, 'file/index.html', {'show':"show", "file_list":file_list})

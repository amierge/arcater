#!/bin/sh

port=3007

pid=$(netstat -tunlp | grep :$port | awk '{print $7}' | awk -F"/" '{ print $1 }');

if [  -n  "$pid"  ];  then
    kill  -9  $pid;
fi

cd web
#gunicorn  web.wsgi:application -b 0.0.0.0:3007
#nohup gunicorn -w 3 -k gevent -b :3007 web.wsgi:application &
nohup gunicorn -w 3 -k gevent -b :3007 web.wsgi:application > ../.out 2>&1 &
cd -
